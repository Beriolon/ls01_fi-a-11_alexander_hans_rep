public class Captain {
	
	private String name;
	private String surname;
	private int captainYears;
	private double gehalt;
	
	// TODO: 3. Fuegen Sie in der Klasse 'Captain' das Attribut 'name' hinzu und
	// implementieren Sie die entsprechenden get- und set-Methoden.
	// TODO: 4. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen
	// initialisiert.
	// TODO: 5. Implementieren Sie einen Konstruktor, der alle Attribute
	// initialisiert.

	public Captain(String n, String sn){
		
		name = n;
		surname = sn;
	}
	
	public Captain(String n, String sn, int cY, double g) {
		
		name = n;
		surname = sn;
		captainYears = cY;
		gehalt = g;
		
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void setSalary(double gehalt) {
		// TODO: 1. Implementieren Sie die entsprechende set-Methode.
		// Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.	
		if(gehalt >= 0) {
			this.gehalt = gehalt;
		}
	}

	public double getSalary() {
		// TODO: 2. Implementieren Sie die entsprechende get-Methoden.
		return this.gehalt;
	}
	// TODO: 6. Implementieren Sie die set-Methode und die get-Methode f�r
	// captainYears.
	// Ber�cksichtigen Sie, dass das Jahr nach Christus sein soll.

	public int getCaptainYears() {
		return this.captainYears;
	}
	
	public void setCaptainYears(int captainYears) {
		if(captainYears>=0) {
			this.captainYears = captainYears;
		}
	}
	
	// TODO: 7. Implementieren Sie eine Methode 'vollname', die den vollen Namen
	// (Vor- und Nachname) als string zur�ckgibt.
	public String vollname() {
		String vollname = this.name + " " + this.surname;
		return vollname;
	}
	
	}