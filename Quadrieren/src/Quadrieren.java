import java.util.Scanner;

public class Quadrieren {
   
	public static void main(String[] args) {
		
		

		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		


		titel();										
		double x = eingabe();			
		double ergebnis = verarbeitung(x);				
		ausgabe(x, ergebnis);							
	}	
	public static void titel() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
	}
	public static void ausgabe(double x, double xquadrat) {
		
		System.out.printf("x = %.2f und x�= %.2f\n", x, xquadrat);
	}
		
	public static double verarbeitung(double x) {

		double ergebnis = x * x;
		return ergebnis;
	}
	
	public static double eingabe() {
	Scanner tastatur = new Scanner(System.in);
	System.out.print("Geben Sie einen x-Wert an: ");
	double x = tastatur.nextDouble();	
	return x	;
	}
}

