
public class Weltraum {


	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
	Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
	Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
	
	Ladung a = new Ladung("Ferengi Schneckensaft", 200);
	Ladung b = new Ladung("Bat'leth Klingonen Schwert", 200);
	Ladung c = new Ladung("Borg-Schrott", 5);
	Ladung d = new Ladung("Rote Materie", 2);
	Ladung e = new Ladung("Plasma-Waffe", 50);
	Ladung f = new Ladung("Forschungssonde", 35);
	Ladung g = new Ladung("Photonentorpedo", 3);
	
	klingonen.addLadung(a);	
	klingonen.addLadung(b);
	romulaner.addLadung(c);
	romulaner.addLadung(d);
	romulaner.addLadung(e);
	vulkanier.addLadung(f);
	vulkanier.addLadung(g);
	
	klingonen.zustandRaumschiff();
	klingonen.ladungsverzeichnisAusgeben();
	
	romulaner.zustandRaumschiff();
	romulaner.ladungsverzeichnisAusgeben();
	
	vulkanier.zustandRaumschiff();
	vulkanier.ladungsverzeichnisAusgeben();
	
	System.out.println("Start der Simulation__________________________________________________________");
	
	klingonen.photonentorpedoSchiessen(romulaner);
	romulaner.phaserkanoneSchiessen(klingonen);
	vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
	klingonen.zustandRaumschiff();
	klingonen.ladungsverzeichnisAusgeben();
	klingonen.photonentorpedoSchiessen(romulaner);
	klingonen.photonentorpedoSchiessen(romulaner);
	klingonen.zustandRaumschiff();
	klingonen.ladungsverzeichnisAusgeben();
	romulaner.zustandRaumschiff();
	romulaner.ladungsverzeichnisAusgeben();
	vulkanier.zustandRaumschiff();
	vulkanier.ladungsverzeichnisAusgeben();
	klingonen.eintraegebroadcastKommunikatorZurueckgeben();
	
	System.out.println("Ende der Simulation__________________________________________________________");
	
	}

}
