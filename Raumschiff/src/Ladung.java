
public class Ladung {

	private String bezeichnung;
	private int menge;
	
	public Ladung() {
		bezeichnung = "Joghurt";
		menge = 100;
	}
	
	public Ladung(String bzn, int m) {
		bezeichnung = bzn;
		menge = m;
	}
	
	public String getbezeichnung() {
		return bezeichnung;
	}
	
	public void setbezeichnung(String bzn) {
		bezeichnung = bzn;
	}
	
	public int getmenge() {
		return menge;
	}
	
	public void setmenge(int m) {
		menge = m;
	}
	
	@Override
	public String toString() {
		return ("\nBezeichnung: " + this.bezeichnung + "|Menge: " + this.menge);
	}
}
