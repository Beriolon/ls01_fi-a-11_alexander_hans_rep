import java.util.ArrayList;
import java.util.Scanner;

public class Raumschiff {


	private int photonentorpedoAnzahl;
	private int energieversongungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		photonentorpedoAnzahl = 1;
		energieversongungInProzent = 100;
		schildeInProzent = 100;
		huelleInProzent = 100;
		lebenserhaltungssystemeInProzent = 100;
		androidenAnzahl = 1;
		schiffsname = "hallo";
	}
	
	public Raumschiff(int ptA, int evIP, int sIP, int hIP, int lesIP, int aA, String sn) {
		photonentorpedoAnzahl = ptA;
		energieversongungInProzent = evIP;
		schildeInProzent = sIP;
		huelleInProzent = hIP;
		lebenserhaltungssystemeInProzent = lesIP;
		androidenAnzahl = aA;
		schiffsname = sn;
	}
	
	public int getphotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	
	public void setphotonentorpedoAnzahl(int ptA) {
		photonentorpedoAnzahl = ptA;
	}
	
	public int getenergieversongungInProzent() {
		return energieversongungInProzent;
	}
	
	public void setenergieversongungInProzent(int evIP) {
		energieversongungInProzent = evIP;
	}
	
	public int getschildeInProzent() {
		return schildeInProzent;
	}
	
	public void setschildeInProzent(int sIP) {
		schildeInProzent = sIP;
	}
	
	public int gethuelleInProzent() {
		return huelleInProzent;
	}
	
	public void sethuelleInProzent(int hIP) {
		huelleInProzent = hIP;
	}
	
	public int getlebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	
	public void setlebenserhaltungssystemeInProzent(int lesIP) {
		lebenserhaltungssystemeInProzent = lesIP;
	}
	
	public int getandroidenAnzahl() {
		return androidenAnzahl;
	}
	
	public void setandroidenAnzahl(int aA) {
		androidenAnzahl = aA;
	}
	
	public String getschiffsname() {
		return schiffsname;
	}
	
	public void setschiffsname(String sn) {
		schiffsname = sn;
	}
	
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		if(getphotonentorpedoAnzahl() == 0) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			setphotonentorpedoAnzahl(photonentorpedoAnzahl -= 1);
			treffer(r);
			nachrichtAnAlle("Photonentorpedo abgeschossen");
		}
	}
	//hi
	public void phaserkanoneSchiessen(Raumschiff r) {
		if(getenergieversongungInProzent() < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			setenergieversongungInProzent(energieversongungInProzent -= 50);
			treffer(r);
			nachrichtAnAlle("Phaserkanone abgeschossen");
		}
	}
	
	private void treffer(Raumschiff r) {
		if(r.getschildeInProzent() >= 50) {
			r.setschildeInProzent(schildeInProzent -= 50);
		} else {
			r.setschildeInProzent(0);
		}
		if(r.getschildeInProzent() == 0) {
			if(r.gethuelleInProzent() >= 50) {
				r.sethuelleInProzent(huelleInProzent -= 50);
				r.setenergieversongungInProzent(energieversongungInProzent -= 50);
			} else {
				r.sethuelleInProzent(0);
				r.setenergieversongungInProzent(0);
			}
		}
		if (r.gethuelleInProzent() == 0) {
			r.setlebenserhaltungssystemeInProzent(0);
			nachrichtAnAlle("Die Lebenserhaltungssysteme wurden vernichtet.");
		}	
	}
	
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}
	
	public ArrayList<String> eintraegebroadcastKommunikatorZurueckgeben(){
		return broadcastKommunikator;
	}
	
	public void photonentorpedosLaden(int ptA) {
		
	}
	
	public void reparaturDurchfuehren(boolean schild, boolean energieversorgung, boolean huelle, int aA) {
		
	}
	
	public void zustandRaumschiff() {
		System.out.println("Schiffsname:" + schiffsname);
		System.out.println("Photonentorpedos:" + photonentorpedoAnzahl);
		System.out.println("Energieversorgung:" + energieversongungInProzent);
		System.out.println("Schilde:" + schildeInProzent);
		System.out.println("H�lle:" + huelleInProzent);
		System.out.println("Lebenserhaltungssysteme:" + lebenserhaltungssystemeInProzent);
		System.out.println("Androiden:" + androidenAnzahl);
		System.out.println(" ");
	}

	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladung der " + getschiffsname() + ":");
		System.out.println(ladungsverzeichnis.toString());
		System.out.println(" ");
		
	}
	
	public void ladungsverzeichnisAufraeumen() {
		
	}
}

