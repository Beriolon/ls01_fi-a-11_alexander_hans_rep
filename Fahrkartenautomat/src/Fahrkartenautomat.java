﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args){
       
    	boolean loop = true;
    	
    	
    	float zuZahlenderBetrag; 
    	float eingezahlterGesamtBetrag = 0.0f;
    	//Kürzel eM = float eingeworfeneMünze;
    	//Kürzel rB =  float rueckgabebetrag;
    	while(loop == true){
    		zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
    		// Geldeinwurf
    	// -----------
       
    		
    			eingezahlterGesamtBetrag = fahrkartenBezahlen(zuZahlenderBetrag);

    			// Fahrscheinausgaben
    			// -----------------
      
    			fahrkartenAusgeben();
    			// Rückgeldberechnung und -Ausgabe
    			// -------------------------------
    			rueckgeldAusgeben(eingezahlterGesamtBetrag, zuZahlenderBetrag);
    		
    	}    
    }
    public static float fahrkartenbestellungErfassen(){
    	Scanner tastatur = new Scanner(System.in);
    	boolean tWloop = true;
        float zZB = 0;
        int aT = 0;
        int tW = 0;
        
        float[] preis = new float[10];
        preis[0] = 2.90f;
        preis[1] = 3.30f;
        preis[2] = 3.60f;
        preis[3] = 1.90f;
        preis[4] = 8.60f;
        preis[5] = 9.00f;
        preis[6] = 9.60f;
        preis[7] = 23.50f;
        preis[8] = 24.30f;
        preis[9] = 24.90f;
        
        String[] bezeichnung = new String[10];
        bezeichnung[0] = "Einzelfahrschein Berlin AB";
        bezeichnung[1] = "Einzelfahrschein Berlin BC";
        bezeichnung[2] = "Einzelfahrschein Berlin ABC";
        bezeichnung[3] = "Kurzstrecke";
        bezeichnung[4] = "Tageskarte Berlin AB";
        bezeichnung[5] = "Tageskarte Berlin BC";
        bezeichnung[6] = "Tageskarte Berlin ABC";
        bezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
        bezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
        bezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
        		
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        for(int i = 0; i<=9; i++) {
        int j = i+1;
        System.out.printf(bezeichnung[i] + " [%.2f EUR] ("+j+")\n", preis[i]);
        }
        
        
        while (tWloop == true) {
        	tW = tastatur.nextInt();
        	if (tW < 1 || tW > 10) {
        		System.out.println("Dies ist keine gültige Option.");
        	} else {
        		tWloop = false;
        		tW -= 1;
        		
        	}        	
        }
        
        for(int i = 0; i<9; i++) {
        	if(i == tW) {
        		zZB = preis[i];
        	}
        }
        
        tWloop = true;
        System.out.println("Wählen Sie 1 bis 10 Tickets aus:");
        
        while (tWloop == true) {
        	aT = tastatur.nextInt();
        	if (aT < 1 || aT > 10) {
        		System.out.println("Dies ist keine gültige Option.");
        	} else {
        		tWloop = false;
        		
        	}
        } 
        return zZB*aT;
        
    }
    
    public static float fahrkartenBezahlen(float zZB) {
    	Scanner tastatur = new Scanner(System.in);
    	float eGB = 0;
        while(eGB < zZB) {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zZB - eGB));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   float eM = tastatur.nextFloat();
     	   if(eM < 0) {
     		   System.out.println("Fehler: Negativer Betrag eingegeben. Bitte geben Sie einen positiven Betrag zwischen 5Ct und 2 Euro ein.");
     	   } else {
     	   eGB += eM;
     	   	}
        }
        return eGB;
    }
    
    public static void fahrkartenAusgeben() {
                 
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(float eGB, float zZB) {
        float rB = eGB - zZB;
        double rBr = runden(rB);
        if(rB > 0.0){
        	System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rBr);
        	System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rBr >= 2.0) { 
            	System.out.println("2 EURO");
            	rBr = runden(rBr -= 2.0);
            }
            while(rBr >= 1.0) { 
            	System.out.println("1 EURO"); 
         		rBr = runden(rBr -= 1.0);
            }
            while(rBr >= 0.5) {
            	System.out.println("50 CENT");
            	rBr = runden(rBr-= 0.5);
            }
            while(rBr >= 0.2) {
            	System.out.println("20 CENT");
            	rBr = runden(rBr -= 0.2);
            }
            while(rBr >= 0.1) {
            	System.out.println("10 CENT");
            	rBr = runden(rBr -= 0.1);
            }
            while(rBr >= 0.05) {
            	System.out.println("5 CENT");
            	rBr = runden(rBr -= 0.05);
            }
        }
        
        
        
            System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
                               "vor Fahrtantritt entwerten zu lassen!\n"+
                               "Wir wünschen Ihnen eine gute Fahrt.");
            for (int i = 0; i < 8; i++)
            {
               System.out.print("=");
               try {
     			Thread.sleep(250);
     		} catch (InterruptedException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
     		}
            }
            System.out.println("\n\n");
    }
    public static double runden(double zahl) {
    	zahl = Math.round(zahl * 100)/100.00;
    	return zahl;
    }
}

