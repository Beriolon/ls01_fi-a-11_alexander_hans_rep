import java.util.Random;
import java.util.Scanner;

public class Twist {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie ein Wort zum mischen ein:");
		String wort = myScanner.next();
		
		String wortAnfang = wort.substring(0,1);
		String wortMitte = wort.substring(1,wort.length()-1);
		String wortEnde = wort.substring(wort.length()-1,wort.length());
		
		wortMitte = Twister(wortMitte);
		
		System.out.println("Das Wort wurde verschlüsselt und lautet jetzt:");
		System.out.println(wortAnfang + wortMitte + wortEnde);
		
		myScanner.close();
	}
	
	public static String Twister(String wm) {
		var r = new Random();
		char[] wortMitte = wm.toCharArray();		 
		
        for (int i = 0; i < wortMitte.length; i++) { 														
            int twisten = r.nextInt(wortMitte.length);														
            char temp = wortMitte[twisten];														
            														
            wortMitte[twisten] = wortMitte[i];														
            wortMitte[i] = temp;
        }
        
        return new String(wortMitte);
	}
	
}
