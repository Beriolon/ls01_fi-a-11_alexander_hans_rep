import java.util.Scanner;

public class Konsoleneingabe {

	public static void main(String[] args) {
		String name;
		int alter;
		float groesse;
		char ort;
		boolean bestaetigen;
		char geschlecht;
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.printf("Hallo! Du bist neu hier, oder? Wer bist du und wie alt bist du? \n");
		System.out.print("Geben Sie Ihren Namen ein: ");
		name = myScanner.next();
		
		System.out.print("Geben Sie Ihr Alter ein: ");
		alter = myScanner.nextInt();
		
		System.out.println("Hallo " + name + ",");
		System.out.println("du bist erst " + alter + "?");
		alter += 1;
		System.out.println("Also mir w�re ja " + alter + " lieber...");
		System.out.printf("Wie gro� bist du denn? \n");
		
		System.out.print("Geben Sie Ihre Gr��e ein: ");
		groesse = myScanner.nextFloat();
		System.out.println("Und dazu bist du auch nur " + groesse + "m gro�?");
		groesse += 0.1f;
		System.out.println("Tut mir leid, aber " + groesse + "m muss man schon gro� sein.");
		
		
		System.out.print("Geben Sie Ihr biologisches Geschlecht (w/m/d) ein: ");
		geschlecht = myScanner.next().charAt(0);
		if(geschlecht == 'w') {
			System.out.printf("Morgen 14 Uhr in deinem Bett dann? ;)\n");
		} else if(geschlecht == 'm') {
			System.out.printf("Na dann lass uns doch irgendwann mal in einer Bar volllaufen lassen.\n");
		} else if(geschlecht == 'd') {
			System.out.printf("Na dann wei� ich ja, mit welchen Fragen ich dich durchl�chern werde.\n");
		} else {
			System.out.printf("Was soll das sein???\n");
		}
		
		System.out.printf("Ist jetzt aber auch egal. in welcher Stadt lebst du denn?\n");
		System.out.print("Geben Sie Ihr Wohnort ein: ");
		ort = myScanner.next().charAt(0);
		System.out.println("Ach, du lebst in " + ort + "? Ist es dort nicht voll teuer?");
		
		System.out.print("Stimmen Sie der Aussage zu oder nicht (true/false): ");
		bestaetigen = myScanner.nextBoolean();
		if(bestaetigen == true) {
			System.out.printf("Es ist also wahr!\n");
		} else 
		{
			System.out.printf("Es war also nur ein Ger�cht...\n");
		}
		
		alter -= 1;
		System.out.println("Ok, ich wiederhole jetzt einfach mal, was du mir gesagt hast. Du hei�t " + name + "und bist " + alter + " Jahre alt. Du bist " + geschlecht + " und lebst in " + ort + ".");
		
	}
}
