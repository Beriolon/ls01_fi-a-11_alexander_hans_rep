import java.util.Scanner;

public class Funktionsloeser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie ein Wert ein:");
		double wert = myScanner.nextDouble();
		
		
		if(wert <= 0){
			wert = exponentiell(wert);
		} else if (0 < wert && wert <= 3) {
			wert = quadratisch(wert);
		} else {
			wert = linear(wert);
		}
		System.out.println("Das Ergebnis lautet "+ wert);
		myScanner.close();
	}
	
	public static double linear(double x) {
		double ergebnis = 2*x+4;
		return ergebnis;
	}
	
	public static double quadratisch(double x) {
		double ergebnis = x*x+1;
		return ergebnis;
	}
	
	public static double exponentiell(double x) {
		double eulersche = 2.718;
		double ergebnis = Math.pow(eulersche, x);
		
		return ergebnis;
	}
}
