import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		// Benutzereingaben lesen
		String artikel = liesString();
		int anzahl = liesInt();
		double nettopreis = liesDouble1();
		double mwst = liesDouble2();
	
		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);		
	}
	public static String liesString() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("was möchten Sie bestellen?");
		String artikel = myScanner.next();
		return artikel;
	}
	  
	public static int liesInt() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();
		return anzahl;
	}

	public static double liesDouble1() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie den Nettopreis ein:");
		double nettopreis = myScanner.nextDouble();
		return nettopreis;
	}
	    
	   
	public static double liesDouble2() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();
		return mwst;
	}
	   
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	   
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	    
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");	
	}
}